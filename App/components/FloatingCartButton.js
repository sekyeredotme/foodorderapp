import React from 'react';
import {TouchableOpacity, StyleSheet, View, Text} from 'react-native';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import * as RootNavigation from './RootNavigation';

const FloatingCardButton = () => {
  const {items, showCart} = useSelector(s => s.cart);
  const cart = items.reduce((sum, item) => (sum = sum + item.qty), 0);

  return (
    <>
      {showCart ? (
        <TouchableOpacity
          style={styles.button}
          onPress={() => RootNavigation.goBack()}>
          <Icon name="times" size={30} />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => RootNavigation.navigate('Cart')}>
          <View style={styles.label}>
            <Text style={styles.text}>{cart}</Text>
          </View>
          <Icon name="shopping-cart" size={30} />
        </TouchableOpacity>
      )}
    </>
  );
};

export default FloatingCardButton;

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    position: 'absolute',
    bottom: 10,
    right: 10,
    height: 70,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  label: {
    borderRadius: 100,
    backgroundColor: 'green',
    width: 24,
    height: 24,
    top: 0,
    right: 0,
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff'
  }
});
