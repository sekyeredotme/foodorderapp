import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Meals from '../containers/Meals';
import MealDetails from '../containers/Meals/MealDetails';
import Checkout from '../containers/Checkout';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator initialRouteName="Meals">
      <Stack.Screen name="Meals" component={Meals} />
      <Stack.Screen
        name="Details"
        component={MealDetails}
        options={({route}) => ({title: route.params.name})}
      />
      <Stack.Screen name="Checkout" component={Checkout} />
    </Stack.Navigator>
  );
};
