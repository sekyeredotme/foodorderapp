const initialState = {
  showCart: false,
  items: [],
};

const cartStore = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      const needle = state.items.findIndex(item => item.id === action.meal.id);
      if (needle >= 0) {
        const currentCartState = state.items.slice();
        const mealToAdd = currentCartState[needle];
        const updatedMeal = {
          ...mealToAdd,
          qty: mealToAdd.qty + 1,
        };
        currentCartState[needle] = updatedMeal;
        return {...state, items: currentCartState};
      } else {
        const updatedMeal = {
          ...action.meal,
          qty: 1,
        };
        return {
          ...state,
          items: [...state.items, updatedMeal],
        };
      }

    case 'REMOVE_FROM_CART':
      return state.items.filter(item => item.id !== action.meal.id);

      // TODO: action for reducing qty of a meal in an order 

    case 'TOGGLE_CART_MODAL':
      return {...state, showCart: !state.showCart};

    default:
      return state;
  }
};

export default cartStore;
