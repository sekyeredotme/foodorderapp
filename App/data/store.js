import {createStore, combineReducers} from 'redux';

import cartStore from './cart/reducer';

const allReducers = combineReducers({
  cart: cartStore,
});

const store = createStore(allReducers);

export default () => {
  return {store};
};
