import React from 'react';
import {View, FlatList} from 'react-native';

import MealList from '../data/meal-list.json';
import MealItem from './Meals/MealItem';

function Meals({navigation}) {
  return (
    <View style={{padding: 15}}>
      <FlatList
        data={MealList}
        renderItem={({item, index}) => (
          <MealItem key={index} item={item} route={navigation} />
        )}
      />
    </View>
  );
}

export default Meals;
