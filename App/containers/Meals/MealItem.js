import React from 'react';
import {View, Text, Button, StyleSheet, Image} from 'react-native';

function MealItem({item, route}) {
  return (
    <View style={styles.container}>
      <View style={styles.item}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{uri: item.image}}
            resizeMode="cover"
          />
        </View>
        <View style={styles.content}>
          <View style={styles.info}>
            <Text style={styles.header}>{item.name}</Text>
            <Text>{item.kitchen}</Text>
            <Text style={styles.price}>GHs {item.price}</Text>
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="Details"
              onPress={() =>
                route.navigate('Details', {meal: item, name: item.name})
              }
            />
          </View>
        </View>
      </View>
    </View>
  );
}

export default MealItem;

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: '#f8f8f8',
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
    marginBottom: 10,
    borderRadius: 10,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
  },
  imageContainer: {
    marginRight: 15,
  },
  image: {
    alignSelf: 'center',
    height: 90,
    width: 90,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    fontSize: 22,
  },
  info: {
    flexDirection: 'column',
  },
  buttonContainer: {
    flexDirection: 'column',
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
