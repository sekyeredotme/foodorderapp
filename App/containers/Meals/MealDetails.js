import React from 'react';
import {StyleSheet, Text, View, Button, Image, Dimensions} from 'react-native';
import {useDispatch} from 'react-redux';

const MealDetails = ({route, navigation}) => {
  const {meal} = route.params;
  const dispatch = useDispatch();
  const meta = {
    id: meal.id,
    name: meal.name,
    price: meal.price,
  };

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{uri: meal.image}}
          resizeMode="cover"
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.header}>{meal.name}</Text>
        <Text>{meal.kitchen}</Text>
        <Text style={styles.price}>{meal.price}</Text>
        <View style={styles.buttonContainer}>
          <Button
            title="Add to Cart"
            onPress={() => dispatch({type: 'ADD_TO_CART', meal: meta})}
          />
        </View>
      </View>
    </View>
  );
};

export default MealDetails;

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flex: 1,
  },
  imageContainer: {
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    position: 'relative',
  },
  image: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: 300,
  },
  content: {
    flex: 1,
  },
  header: {
    fontSize: 34,
  },
  price: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  buttonContainer: {
    marginTop: 35,
  },
});
