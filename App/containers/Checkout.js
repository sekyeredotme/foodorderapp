import React from 'react';
import {StyleSheet, Text, View, Button, TextInput} from 'react-native';
import {useSelector} from 'react-redux';
import {Formik} from 'formik';

const Checkout = () => {
  const {items} = useSelector(s => s.cart);
  const total = items.reduce(
    (sum, {price, qty}) => (sum = sum + eval(price * qty)),
    0,
  );

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Please fill in your information.</Text>

      <Formik
        initialValues={{firstname: '', lastname: '', email: '', location: ''}}
        onSubmit={values => console.log(values)}>
        {({handleChange, handleBlur, handleSubmit, values}) => (
          <View style={styles.formContainer}>
            <TextInput
              onChangeText={handleChange('firstname')}
              onBlur={handleBlur('firstname')}
              placeholder="First Name"
              value={values.firstname}
              style={styles.textInput}
            />
            <TextInput
              onChangeText={handleChange('lastname')}
              onBlur={handleBlur('lastname')}
              placeholder="Last name"
              value={values.lastname}
              style={styles.textInput}
            />
            <TextInput
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              placeholder="Email"
              value={values.email}
              style={styles.textInput}
            />
            <TextInput
              onChangeText={handleChange('location')}
              onBlur={handleBlur('location')}
              placeholder="Delivery Location"
              value={values.location}
              style={styles.textInput}
            />
            <View style={styles.submitButton}>
              <Button onPress={handleSubmit} title="Submit" />
            </View>
          </View>
        )}
      </Formik>
    </View>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    justifyContent: 'center',
    padding: 15,
  },
  heading: {
    marginBottom: 15,
  },
  formContainer: {},
  submitButton: {
    marginTop: 15,
  },
  textInput: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    marginBottom: 15,
  },
});
