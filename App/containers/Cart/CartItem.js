import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const CartItem = ({items: {name, price, qty}}) => {
  const total = qty * price;
  return (
    <View style={styles.container}>
      <View style={styles.name}>
        <Text style={styles.nameText}>{name}</Text>
      </View>
      <View style={styles.meta}>
        <Text>{price}</Text>
      </View>
      <View style={styles.meta}>
        <Text>{qty}</Text>
      </View>
      <View style={[styles.meta, styles.totalColumn]}>
        <Text style={styles.total}>GHs {total.toFixed(2)}</Text>
      </View>
    </View>
  );
};

export default CartItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  meta: {
    width: 50,
  },
  name: {
    flex: 1,
  },
  nameText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  totalColumn: {
    flex: 0.4,
    flexWrap: 'wrap',
  },
  total: {
    fontWeight: 'bold',
  },
});
