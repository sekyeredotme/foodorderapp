import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {StyleSheet, Text, View, FlatList, Button} from 'react-native';
import CartItem from './Cart/CartItem';

const Cart = ({navigation}) => {
  const dispatch = useDispatch();
  const {items} = useSelector(s => s.cart);
  const total = items.reduce(
    (sum, item) => (sum = sum + (item.qty * item.price)),
    0,
  );

  useEffect(() => {
    dispatch({type: 'TOGGLE_CART_MODAL'});

    return () => {
      dispatch({type: 'TOGGLE_CART_MODAL'});
    };
  }, []);

  return (
    <View style={styles.modal}>
      <Text style={styles.text}>Shopping Cart</Text>

      <View style={styles.itemGroup}>
        <FlatList
          data={items}
          renderItem={({item, index}) => <CartItem items={item} />}
        />
      </View>

      <View style={styles.buttonContainer}>
        <View style={styles.total}>
          <Text style={styles.totalText}>GHs {total.toFixed(2)}</Text>
        </View>
        <Button
          title="Checkout"
          onPress={() => navigation.navigate('Checkout')}
        />
      </View>
    </View>
  );
};

export default Cart;

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 100,
  },
  text: {
    fontSize: 30,
  },
  buttonContainer: {
    flex: 0,
    marginBottom: 100,
    paddingTop: 20,
  },
  itemGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  total: {
    alignContent: 'center',
    marginBottom: 20,
    alignItems: 'center',
  },
  totalText: {
    fontWeight: 'bold',
    fontSize: 32,
  },
});
