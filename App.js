import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';

import configureStore from './App/data/store';
import Router from './App/components/router';
import {navigationRef} from './App/components/RootNavigation';
import FloatingCartButton from './App/components/FloatingCartButton';
import Cart from './App/containers/Cart';

const RootStack = createStackNavigator();

export default function App() {
  const {store} = configureStore();

  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <RootStack.Navigator mode="modal" headerMode="none">
          <RootStack.Screen name="Router" component={Router} />
          <RootStack.Screen name="Cart" component={Cart} />
        </RootStack.Navigator>
        <FloatingCartButton />
      </NavigationContainer>
    </Provider>
  );
}
